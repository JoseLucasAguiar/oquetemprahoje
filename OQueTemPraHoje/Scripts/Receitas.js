﻿
$(function () {
    $('#btnAddIngrediente').click(function (e) {
        e.preventDefault();
        $("#teste").append($("<div class='form-group' id='ingredientes'><input type='text' name='ingrediente' class='form-control'/></div>"));
    });

    $('#addIngrediente').click(function (e) {
        e.preventDefault();
        $('#insereDiv').append($("<div class='form-group'><input type='text' placeholder='Insira um ingrediente por linha' class='form-control' name='ingredientes'/></div>"));
    });

    $('#addIngredienteParaEdicao').click(function (e) {
        e.preventDefault();
        $('#insercaoDeIngredientes').append($("<div class='form-group'><input type='text' placeholder='Insira um ingrediente por linha' class='form-control' name='ingredientes'/></div>"));
    })
});

function addCamposIngredientesEditor(ingredientes) {
    var ingr = ingredientes.split(",");

    for (var i = 0; i < ingr.length; i++) {
        $('#insercaoDeIngredientes').append($("<div class='form-group'><input type='text' class='form-control' name='Ingredientes' id='oldTitulo' value=" + ingr[i] + " /></div>"));
    }
}

function validaFormDeInsercao(event) {
    alert(event.target);
    var titulo = $(event.target).find('#titulo_receita').val();
    var descricao = $(event.target).find('descricao_receita').val();
    var ingredientes = $(event.target).find('ingrediente_receita').val();

    return !(titulo === null || titulo === "" || descricao === null || descricao === "" || ingredientes === null || ingredientes === "");
}

function excluir(id, url) {
    $.post({
        url: url,
        data: { id: id }
    });
}


function buscaReceitaPorID(id) {

    alert($('#DeleteReceita').data('request-url'));

    $.post({
        //url: "/Controllers/BuscaReceita/Excluir",
        url: $('#DeleteReceita').data('request-url'),
        data: {
            id: id,
            url: "Receitas"
        }
    }).done(function (paginaNova) {
        window.location.href = paginaNova;
    });
}

