﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace OQueTemPraHoje.Models
{
    public class Receita
    {
        public int ID { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Ingredientes { get; set; }
    }

    public class ReceitaDbContext : DbContext
    {
        public DbSet<Receita> Receitas { get; set; }
    }
}