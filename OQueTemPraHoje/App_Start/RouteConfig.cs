﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OQueTemPraHoje
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Receitas", action = "Receitas", id = UrlParameter.Optional }
               );
        }
    }
}
//url: "{controller}/{action}/{id}",
//defaults: new { controller = "Receitas", action = "Receitas", id = UrlParameter.Optional }
