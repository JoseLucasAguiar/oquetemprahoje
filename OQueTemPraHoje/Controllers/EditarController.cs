﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OQueTemPraHoje.Models;

namespace OQueTemPraHoje.Controllers
{
    public class EditarController : Controller
    {
        private ReceitaDbContext db = new ReceitaDbContext();

        // GET: Editar
        public ActionResult EditarReceita(int? id)
        {
            if (id == null) 
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Receita r = db.Receitas.Find(id);
            if (r == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(r);
        }
        
        public ActionResult AtualizaValores(int? id, string Descricao, string[] Ingredientes, string Titulo)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.ExpectationFailed);

            Receita r = db.Receitas.Find(id);
            if (r == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            string ingr = "";
            string s = "";

            for (int i = 0; i < Ingredientes.Length; i++)
            {
                s = Ingredientes[i];

                if ((s != null || s != ""))
                {
                    ingr += s.ToLower();
                    if (!(i == Ingredientes.Length - 1))
                        ingr += ',';
                }
            }

            r.Descricao = Descricao;
            r.Ingredientes = ingr;
            r.Titulo = Titulo;

            db.SaveChanges();
            
            return RedirectToAction("Receitas", "Receitas");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
