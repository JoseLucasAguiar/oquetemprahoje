﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OQueTemPraHoje.Models;

namespace OQueTemPraHoje.Controllers
{
    public class BuscaReceitaController : Controller
    {
        private ReceitaDbContext db = new ReceitaDbContext();

        // GET: BuscaReceita
        public ActionResult BuscaReceita()
        {
            return View(db.Receitas.ToList());
        }

        // BUSCA RECEITA PARA EXIBIT NO MODAL
        public ActionResult Detalhes(int id)
        {
            Receita r = db.Receitas.Find(id);
            return View(r);
        }


        public ActionResult Excluir(int? id, string actionName)
        {
            if (id == null)
                return RedirectToAction(actionName, db.Receitas.ToList());

            Receita r = db.Receitas.Find(id);
            if (r == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            db.Receitas.Remove(r);
            db.SaveChanges();

            return RedirectToAction(actionName, actionName);//"Receitas", "Receitas");//View("Receitas", db.Receitas.ToList());
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
