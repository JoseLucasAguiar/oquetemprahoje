﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OQueTemPraHoje.Models;

namespace OQueTemPraHoje.Controllers
{
    public class ReceitasController : Controller
    {
        private ReceitaDbContext db = new ReceitaDbContext();

        // GET: Receitas
        public ActionResult Receitas()
        {
            return View(db.Receitas.ToList());
        }

        public ActionResult BuscaReceita()
        {
            return View(db.Receitas.ToList());
        }

        [HttpPost]
        public ActionResult BuscaReceitaPorIngrediente(string[] ingrediente)
        {
            if (ingrediente == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var query = db.Receitas.AsQueryable();

            foreach (string s in ingrediente)
            {
                if (String.IsNullOrWhiteSpace(s)) continue;

                query = query.Where(r => r.Ingredientes.ToLower().Contains(s.ToLower()));
            }

            return View("BuscaReceita", query.ToList());
        }

        public ActionResult InsereReceita(string titulo, string descricao, string[] ingredientes)
        {
            if (String.IsNullOrWhiteSpace(titulo) || String.IsNullOrWhiteSpace(descricao) || ingredientes == null || ingredientes.Length == 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            string ingr = "";
            string s = "";

            for(int i = 0; i < ingredientes.Length; i++)
            {
                s = ingredientes[i];

                if( (s != null || s != ""))
                {
                    ingr += s.ToLower();
                    if (!(i == ingredientes.Length - 1))
                        ingr += ',';
                }
                
            }

            db.Receitas.Add(new Receita() { Titulo = titulo, Descricao = descricao, Ingredientes = ingr });
            db.SaveChanges();

            return new RedirectResult("Receitas");
        }


        // GET: Receitas/Create
        public ActionResult Create()
        {
            return View();
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
